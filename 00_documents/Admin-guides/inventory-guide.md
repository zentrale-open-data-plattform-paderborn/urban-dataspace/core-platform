## Inventory Guide
This guide is organized from the top of the inventory to the bottom and has the goal to give a complete description on each fields, as well as provide exampled where it is suitable.

## Preface
Basic k8s knowledge (what is a namespace, a storage class, an operator) is needed in order to progress through this document.

## Where to start
In the repository is a [default inventory](../../02_core_platform/default_inventory.yml) file located with hints and description.<br>
Create a local copy and start filling the variables.

> Since this file can be used for the setup of the base-cluster as well, the first portion of the file created during the [Cluster Setup](cluster.md) can be salvaged.

## Configuration
We start right after the portion of the base-cluster setup.
### localhost setup
This part is configured to match the machine the ansible-playbook is executed on. If you run it from your local machine, you only need to verify the location of your python3 binary.
```yaml
    controller:
      hosts:
        localhost:
          ansible_host: 127.0.0.1
          ansible_connection: local
          ansible_python_interpreter: /usr/bin/python3
```
For example on a UNIX machine the `which` command will give you this information:
```bash
$ which python3
/usr/bin/python3
```

### eMail Server
Configuration and access to the eMail Server to be used
```yaml
          server: "<email_server>"
          user: "<user>"
          password: "<password>"
          email_from: "<email-from>"
```
> The field `email_from` will be used for components which can/shall send eMails to users. For example password reset email from Keycloak.

### k8s settings
```yaml
        inv_k8s:
          config:
            context: "microk8s"
          ingress_class: public
```
The fields `context` and `ingress_class` are cluster specific. The `context` is found inside the kubeconfig and the `ingress_class` is known after an ingress controller is installed on the cluster.
> The presented setting is valid for a cluster setup following the base-cluster repository.

### Storage classes
```yaml
          storage_class:
            rwo: "microk8s-hostpath"
            rwx: "microk8s-hostpath"
            loc: "microk8s-hostpath"
            provisioner: "microk8s.io/hostpath"
```
Provisioner and name of the storage classes to be used.
> The presented setting is valid for a cluster setup following the base-cluster repository.

### GitLab deployment access
```yaml
          gitlab_access:
            user_email: "<email>"
            user: "<user>"
            token: "<token>"
```
Any user and a [deploy/access-token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) which shall be used during deployment (i.e. pulling images from gitlab registry)

### Cluster wide Cert Manager Settings
For issuing certificates the k8s resources need to define which CA they want to issue. You can give any name and eMail address here.
```yaml
          cert_manager:
            le_email: "<email>"
            staging_issuer_name: "<letsencrypt-staging>"
            prod_issuer_name: "<letsencrypt-production>"
```
> By using the `{{ VARIABLE }}`- notation you can make a reference to a variable you filled earlier. i.e. ìn case of the `le_mail:
```yaml
          cert_manager:
            le_email: "{{ inv_email.email_from }}"
            staging_issuer_name: "letsencrypt-staging"
            prod_issuer_name: "letsencrypt-production"
```

### Remark
For the following sections a few fields are reoccurring:
- `enable`: This is general flag to enable or disable a section of the deployment/configuration. 
- `ns_create`: This flag tells the script if the k8s-namespace shall be created<br>
(set it to false, if you already have namespaces setup)
- `ns_name`: The desired k8s-namespace for related components to be deployed.
- `ns_kubeconfig`: The kubeconfig for the specific namespace. <br>
(If you have a general kubeconfig for the cluster, you can use `{{ kubeconfig_file }}`, which refers to the previously defined variable from the cluster-setup)

### Managed databases
This portion describes what is expected to be found on the cluster, if you want the hoster to provide managed databases for the platform. If this section is enabled, during deployment no databases will be deployed for the selected components:
>Only one example for Postgres and one example for Mongo is shown, since it is the same for the rest.
```yaml
        inv_mngd_db:
          enable: "true"
          ns_kubeconfig: "{{ kubeconfig_file }}"
          keycloak_postgres:
            enable: "true"
            ns_name: "<namespace for secret>"
            db_address: "<database-service.namespace.svc.cluster.local>"
            db_name: "<database name>"
            user_k8s_secret: "<k8s secret name; database keycloak user>"
            admin_k8s_secret: "<k8s secret name; database postgres/admin user>"
          gravitee_mongo:
            enable: "true"
            ns_name: "<namespace for secret>"
            db_address: "<database-service.namespace.svc.cluster.local>"
            admin_k8s_secret: "<k8s secret name; database admin user>"
```
In order to run the components with managed databases several information need to be available during deployment:
1. Postgres<br>
`ns_name`: The name of the k8s-namespace the secrets are stored for accessing the databases.<br>
`db_address`: The "path" of the k8s-service pointing to the database.<br>
`db_name`: The given name of the database inside the Postgres cluster.<br>
`user_k8s_secret`: The name of the k8s-secret containing the user credentials for database usage. <br>
`admin_k8s_secret`: The name of the k8s-secret containing the database-admin credentials.
2. Mongo<br>
Same as Postgres (just less information needed)

### Operator Stack
The operator stack will be the place, where all management components will live. You can disable components which are provided by your hoster. One frequently met candidate could be the cert-manager.
```yaml
        ## Operator stack
        inv_op_stack:
          ns_create: "true"
          ns_name: "cro-operator-stack"
          ns_kubeconfig: "{{ kubeconfig_file }}"
          cert_manager_operator:
            enable: "true"
            ns_name: "cro-operator-stack"
          postgres_operator:
            enable: "true"
            ns_name: "cro-operator-stack"
            logical_backup:
              enabled: false
              access_key: ""
              bucket: ""
              endpoint: ""
              region: ""
              secret: ""
              schedule: ""
          minio_operator:
            enable: "true"
            ns_name: "cro-operator-stack"
          keycloak_operator:
            enable: "true"
            ns_name: "cro-operator-stack"
          keel_operator:
            enable: "true"
            ns_name: "cro-operator-stack"
            admin: "admin@{{ DOMAIN }}"
            password: "<create-keel-password>"
```
The postgres operator has a section `logical_backup`. This section enables you to link the operator to any S3 storage by adding your information for the bucket, where logical postgres-db backups shall be stored.
Later in this file you find a parameter `create_logical_backup`, which lets you decided if you specifically want to dis-/enable a backup for a database.<br>
**Note:** The `schedule` field follows the cronjob notation. In order to create/check your schedule you can use this website: [crontab.guru](https://crontab.guru/)

In the section `keel_operator` you need to set a password, which can be used to access the user interface if needed.
```bash
        operator_stack:
        ...
          velero:
            enable: "false"
            ns_name: "cro-operator-stack"
            velero_namespace_suffix: "backup"
            backup:
              location_name: ""
              access_key: ""
              bucket: ""
              region: ""
              endpoint: ""
              secret: ""
```
The section `velero` enables a backup-restore tool which backups complete namespaces. Same as above, you may link it to any S3 storage by adding your inforamtion, which will be used to store these backups. The [official documentation](https://velero.io/docs/v1.8/) of velero can guide you through processes, like [restoring a namespace](https://velero.io/docs/v1.3.0/restore-reference/)
```bash
        operator_stack:
        ...          
          mongo_backup:
            enable: "false"
            ns_name: "cro-operator-stack"
            ns_kubeconfig: "{{ kubeconfig_file }}"
            backup:
              access_key: ""
              bucket: ""
              endpoint: ""
              secret: "" 
              mongo-uri: ""

```
The section `mongo` enables logical backups like done by the psotgres operator. Currently the only used mongo database, which would profit from the backup is the mongo from orion, which needs to be reflected in the `mongo-url` field, constructed like this:
```bash
  mongo-uri: "mongodb://{{MONGO_DB_ADMIN_USERNAME}}:{{MONGO_DB_ADMIN_PASSWORD}}@{{ORION_MONOG_POD_NAME}}.{{ORION_MONGO_NAMESPACE}}:27017/orion"
```



### Identiy Management Stack
This portion will configure the deployment and [Keycloak-realm](https://www.keycloak.org/docs/latest/server_admin/#core-concepts-and-terms) of the platform. The `realm_name` will be visible for user upon login.
```yaml
        ## Identity management stack
        inv_idm:
          ns_create: "true"
          ns_name: "{{ ENVIRONMENT }}-idm-stack"
          db_ns_name: "{{ ENVIRONMENT }}-idm-db-stack"
          ns_kubeconfig: "{{ kubeconfig_file }}"

          ## IDM Users
          platform:
            admin_first_name: "Admin"
            admin_surname: "Admin"
            admin_email: "admin@{{ DOMAIN }}"
            master_username: "admin@{{ DOMAIN }}"
            master_password: "<password>"
          ## IDM General Values
          keycloak:
            realm_name: "<realm>"
            k8s_secret_name: "<idm-secret-name>"
            scope: "openid"
            log_level: "INFO"
            theme: "keycloak"
            #theme_url: "<theme-url>" # Use this parameter for the theme extension
            enable_logical_backup: false
          group:
            default_tenant_name: "default_dataspace"
```
The `db_ns_name` refers to the namespace, the Postgres instances for Keycloak are living. If no managed databases are used for Keycloak, this field defines the desired namespace.<br>
The `platform` section defines the general admin user for the platform, as well as the configuration of Keycloak itself.
> The password restrictions for Keycloak are configured with orientation to BSI recommendation. (Length 12 with at least 1 Uppercase, 1 lowercase, 2 special characters, 1 digit)

The `keycloak` section describes the realm configuration. The field `idm-secret-name` describes the k8s-secret which is used to store the admin credentials.
> It is possible to customize the visuals of Keycloak using themes. In order to persistently deploy a theme, you need to provide a link to an extension containing this theme in the field `theme_url`. If you want to create a theme, the [official documentation](https://www.keycloak.org/docs/latest/server_development/#_themes) is a good starting point.

The field `default_tenant_name` is used for the Orion and acts as fiware-service. It is used to define dataspaces on the platform. Give it any name suitable for your first dataspace. A guide on how to create additional dataspaces is located in the [documentation](../keycloak/create_tenant.md).

### API Management
The visible part of the API Management configuration is only the name of the Application which is holds the APIs. 
```yaml
        inv_am:
          apim:
            enable: "true"
            ns_create: "true"
            ns_name: "{{ ENVIRONMENT }}-apim-stack"
            ns_kubeconfig: "{{ kubeconfig_file }}"
            management_log_level: "INFO"
            gateway_log_level: "INFO"
            default_basic_auth: "<basic auth>"
            portal_application: "Portal Application"
```
The field `default_basic_auth` is a base64 string containing the credentials used during deployment (they will be disabled after deployment, with the integration of Keycloak as [identity provider](https://docs.gravitee.io/am/1.x/am_quickstart_authenticate_users.html)). The [default for Gravitee](https://docs.gravitee.io/apim/3.x/apim_quickstart_console_login.html) would be:<br>
```yaml
            default_basic_auth: "Basic YWRtaW46YWRtaW4="
```

### Context Management
For the context management section you generally only have to choose username and passwords to your linking and choose if you want to enable/disable one of the CM systems (both can be used at the same time).
```yaml
        ## Context Management
        inv_cm:
          fiware:
            context_mgmt_enable: "true"
            context_mgmt_ns_create: "true"
            context_mgmt_ns_name: "{{ ENVIRONMENT }}-context-management-stack"
            ns_kubeconfig: "{{ kubeconfig_file }}"
          mongo:
            initdb_database: "orion"
            initdb_root_username: "<username>"
            initdb_root_password: "<password>"
          orion:
            mongodb_user: "<mongodb-user>"
            mongodb_password: "<mongodb-password>"
          frost:
            context_mgmt_enable: "true"
            context_mgmt_ns_create: "true"
            context_mgmt_ns_name: "{{ ENVIRONMENT }}-context-management-stack"
            ns_kubeconfig: "{{ kubeconfig_file }}"
            context_mgmt_mqtt_enable: "true"
            context_mgmt_db_username: "<db-username>"
            context_mgmt_db_password: "<db-password>"
            enable_logical_backup: false
```

> Regarding the FROST Server. Currently this integration is **not** ready for production and needs be tested further. 

### General OAuth Proxy
The OAuth Proxy acts a bridge for components which do not support the OIDC protocol for user access control. This proxy will be placed in front of each of these components.
```yaml
        inv_pub:
          general_oauth_proxy:
            enable: "true"
            ns_create: "true"
            ns_name: "{{ ENVIRONMENT }}-public-proxy-stack"
            ns_kubeconfig: "{{ kubeconfig_file }}"
            ### server specific cookie for the secret; create a new one with `openssl rand -base64 32 | head -c 32 | base64`
            cookie_secret: "<cookie_secret>"
```
The field `cookie_secret` is used to create randomness inside the cookie, used to access protected websites. It ensures the avoidance of overlapping when accessing different systems using this proxy.<br>
You can generate it using the command shown in the default_inventory.yaml:
```bash
$ openssl rand -base64 32 | head -c 32 | base64
RkVFeHpIWEFyeWxNckFKeWxrSjlpZVh5Z0NxT1pGa3A=
```

### Data Management Stack
The data management stack combines tools for holding and managing data stored in the platform. Taking a look at them one at a time:
```yaml
        inv_dm:
          ns_create: "true"
          ns_name: "{{ ENVIRONMENT }}-data-management-stack"
          ns_kubeconfig: "{{ kubeconfig_file }}"
```
This timescale switch is used to disable the deployment of the Postgres cluster if you are using managed databases. 
```yaml
          timescale:
            enable: "true"
            enable_logical_backup: false
```
This is the first Grafana of the platform, responsible for data in i.e. Timescale.
```yaml
          grafana:
            enable: "true"
            admin: "<admin>"
            password: "<password>"
            rbac:
              create: false
              psp_enabled: false
              psp_use_app_armor: false
            service_account:
              create: false
            datasources:
              timescale_name: "TimescaleDB"
```
Set username and password (`admin`, `password`) for Grafanas internal admin. The scripts will integrate Grafana user management automatically with Keycloak.

The MinIO is the platforms hosted s3 compatible object storage.
```yaml
          minio:
            tenant:
              enable: "true"
              accesskey: "<access-key>"
              secretkey: "<secret-key>"
              console_pbkdf_passphrase: "<passphrase>"
              console_pbkdf_salt: "<salt>"
              console_access_key: "<console-access-key>"
              console_secret_key: "<console-secret-key>"
```
For MinIO the name `access-key` acts like a username. Set it something applicable and set passwords for the remaining field.

PGAdmin is an administration tool with a web UI, which can be used to look a the Postgres Clusters on the platform.
```yaml
          pgadmin: 
            enable: "true"
            default_email: "admin@{{ DOMAIN }}"
            default_password: "<yourpassword>"
            config_enhanced_cookie_protection: "True"
```

### Monitoring Stack
It is possible to deploy a monitoring stack with basic k8s dashboards for performance, usage, traffic and so on. You may coordinate with your k8s hoster, if such a stack is already in place. In that case you can disable the components.
```yaml
        inv_ml:
          ns_create: "true"
          ns_name: "{{ ENVIRONMENT }}-monitoring-logging-stack"
          ns_kubeconfig: "{{ kubeconfig_file }}"
          grafana:
            enable: "true"
            admin: "<admin>"
            password: "<password>"
          loki:
            enable: "true"
```

### Replica Config
In this section you can configure the replication count a few components. This section will grow in the future. For now if you want to adjust the replica count of another component, please refer to the deployment template [of the component](https://gitlab.com/urban-dataspace-platform/core-platform/-/tree/master/02_core_platform/templates).
```yaml
        inv_replicas:
          keycloak:
            db: 2
            app: 2
          gravitee:
            api_mgmt: 1
            gateway: 1
          frost:
            db: 1
          timescale:
            db: 1
```
> This settings will only be taken into account, with the deployment is enabled for the related component.
